
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class WxController implements Initializable
{
   @FXML
   private TextField zipEnter;

   @FXML
   private Button load;
   
   @FXML
   private Button clear;
   
   @FXML
   private Label CityState;
   
   @FXML
   private Label Weather;
   
   @FXML
   private Label Temperature;
   
   @FXML
   private Label Wind;
   
   @FXML
   private Label Time;
   
   @FXML
   private Label Pressure;
   
   @FXML
   private Label Visability;
   
   @FXML
   private ImageView Image;
   
   @FXML
   private void handleButtonAction(ActionEvent e) 
   {
      WxModel W = new WxModel();

      // Clear is pressed
      if (e.getSource() == clear)
      {
        zipEnter.setPromptText("Enter a ZIP Code");
        CityState.setText("");
        Weather.setText("");
        Temperature.setText("");
        Wind.setText("");
        Time.setText("");
        Pressure.setText("");
        Visability.setText("");
        Image.setImage(null);
      }
      else {
      if (e.getSource() == load)
      //Load has been pressed
      {
        String zipcode = zipEnter.getText();
        if (W.getWx(zipcode))
        {
            CityState.setText((W.getLocation()));
            Weather.setText((W.getWeather()));
            Temperature.setText(String.valueOf(W.getTemp() + " F"));
            Wind.setText(W.getWind());
            Time.setText(W.getTime());
            Pressure.setText(W.getPressure() + " MB");
            Visability.setText(W.getVis() + " Miles");
            Image.setImage(W.getImage());
            
     // image.setImage(W.getImage());
        }
        else
        {
            CityState.setText("ERROR");
            Weather.setText("");
            Temperature.setText("");
            Wind.setText("");
            Time.setText("");
            Pressure.setText("");
            Visability.setText("");

         }
      }
      }
   }
   
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
    // TODO
    }    

}