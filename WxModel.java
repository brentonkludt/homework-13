/*import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

public class WxModel
{
//Instances
public static String city;
public static String location;

public WxModel()
{
   this.city = "";
}

public String getCity()
{
   return this.city;
}

public void setCity(String c)
{
   this.city = c;
}

/*//*/getters
public String getCity()
{
   return this.city;
}  

//setter
public void setCity(String c)
{
   this.city = c;
}*//*

   public String getWeather(String zip)
	{
	 JsonElement jse = null;
    String shortURL = null;
    
    String newLine   = System.getProperty("line.separator");
    String city      = "";   
    String state     = "";
    String temp      = "";
    location         = "Location: " + city + ", " + state;
    String time      = "";
    String weather   = "";
    String tempature = "";
    String wind_s    = "";
    String pressure  = "";
    String total     = "";

		try
		{
			// Construct Bitly API URL
			URL zipURL = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/geolookup/q/" + zip + ".json");


			// Open the URL
			InputStream is = zipURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{

         if( !jse.getAsJsonObject().get("response").getAsJsonObject().has("error"))        
         { 
            city = jse.getAsJsonObject().get("location").getAsJsonObject().get("city").getAsString();
            state = jse.getAsJsonObject().get("location").getAsJsonObject().get("state").getAsString();
         }
         else
         {
            System.out.println("Please enter a correct zip code!");
         }
      }

if (!jse.getAsJsonObject().get("response").getAsJsonObject().has("error"))
{
      //Second url for city / state...
      try
		{
			// Second URL for zip->city conversion
         URL cityURL = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/conditions/q/" + state + "/" + city + ".json");

			// Open the URL
			InputStream is = cityURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

      if (jse != null)
		{
         location  = "Location:       " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
         time      = "Time:           " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();
         weather   = "Weather:        " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
         tempature = "Tempature F:    " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString();
         wind_s    = "Wind:           " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_string").getAsString();
         pressure  = "Pressure inHG:  " + jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("pressure_mb").getAsString();
              
         total     = location + newLine + time + newLine + weather + newLine + tempature + newLine + wind_s + newLine + pressure;
      }
      
}
return total;

}
}
*/

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;

public class WxModel {
  private JsonElement jse;
  public boolean getWx(String zip)
  {
    try
    {
      URL wuURL = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/conditions/q/" + zip + ".json");

      // Open connection
      InputStream is = wuURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse = new JsonParser().parse(br);

      // Close connection
      is.close();
      br.close();
    }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }

    // Check to see if the zip code was valid.
    return isValid();
  }

  public boolean isValid()
  {
    // If the zip is not valid we will get an error field in the JSON
    try {
      String error = jse.getAsJsonObject().get("response").getAsJsonObject().get("error").getAsJsonObject().get("description").getAsString();
      return false;
    }

    catch (java.lang.NullPointerException npe)
    {
      // We did not see error so this is a valid zip
      return true;
    }
  }

  public String getLocation()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
  }
  
  public String getWeather()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
  }


  public double getTemp()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsDouble();
  }

  public String getWind()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_string").getAsString();
  }
  
    public String getTime()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();
  }
  
    public String getPressure()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("pressure_mb").getAsString();
  }
  
    public String getVis()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("visibility_mi").getAsString();
  }
  
  
  public Image getImage()
  {
    String iconURL = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon_url").getAsString();
    
    return new Image(iconURL);
  }
}
